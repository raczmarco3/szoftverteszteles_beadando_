import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculatorTest {
  private Calculator calculator;

  @BeforeEach
  public void setUp() {
    calculator = new Calculator();
  }

  @Test
  public void testOsszeadas() {
    assertEquals(12, calculator.osszeadas(8, 3), "öszzeadás teszt");
  }

  @Test
  public void testKivonas() {
    assertEquals(12, calculator.kivonas(22, 10), "osztás teszt");
  }

  @Test
  public void testParos() {
    assertEquals(true, calculator.paros(2), "páros teszt");
  }

  @Test
  public void testParatlan() {
    assertEquals(false, calculator.paros(3), "páratlan teszt");
  }

  @Test
  public void testHatvany() {
    assertEquals(8, calculator.hatvany(2, 3), "hatvány teszt");
  }
}
